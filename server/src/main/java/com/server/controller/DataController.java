package com.server.controller;

import com.server.model.SomeEntity;
import com.server.service.SomeEntityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@CrossOrigin
@RestController
public class DataController {

    private final SomeEntityService firstEntityService;

    @Autowired
    public DataController(SomeEntityService firstEntityService) {
        this.firstEntityService = firstEntityService;
    }

    @RequestMapping(value = "/first", method = GET)
    @ResponseBody
    public List<SomeEntity> getFirst() throws IOException {
        return firstEntityService.findAllFirstEntity();
    }

}