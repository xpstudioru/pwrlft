package com.server.service;

import com.server.model.SomeEntity;

import java.io.IOException;
import java.util.List;

public interface SomeEntityService {
    List<SomeEntity> findAllFirstEntity() throws IOException;
}
