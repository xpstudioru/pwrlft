package com.server.repository;

import com.server.model.SomeEntity;

import java.io.IOException;
import java.util.List;

public interface SomeEntityRepository {
    List<SomeEntity> findAllFirstEntity() throws IOException;
}
